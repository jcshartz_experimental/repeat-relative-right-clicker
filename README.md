# Repeat Relative Right-clicker #

**Pardon my dust**

This is a super early project of mine. Basically I wanted a way to activate scripts by right-clicking a location through windows, selecting a script or interactively (through some gui) a script, and running said script. 

## Current goals/plans of this project ##

+ Python, Bash, and Windows CMD script supported
+ GUI will probably be tinker or the like
+ CLI and GUI should allow for selecting multiple files
+ CLI and GUI should allow to select a file relative to the target
+ CLI and GUI should allow pattern matching

## Someday goals/plans of this project ##

+ Installer for people that place way too much trust in me

## Warranty ##

**NONE**

It blows up your computer it's not on me. This program is mostly for my own personal use. **Knowledgable users** should be able to use and adapt it for their own purposes and maybe send some improvements back my way. 

## License ##

[MIT](https://opensource.org/licenses/MIT)

Copyright 2019 Jay Shartzer

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


## Reference ##

I've taken inspiration from the following sources:

+ This [Stack Exchange answer](https://superuser.com/a/1070883) involving adding batch script actions to right click menus

